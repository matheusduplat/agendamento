<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
   
    <link rel="stylesheet" href="{{asset('css/fa_icon/font-awesome.min.css')}}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/Menu/menu.css') }}" rel="stylesheet">
</head>
<header>
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light menu"> 
    

       <ul class="nav">     
        <li class="nav-item dropdown menuUser">
            <a class="nav-link nomeUser" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{auth::user()->name}}
                <i class="fa fa-angle-left" aria-hidden="true"></i>            
        </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item menuUserOp" href="#">Gerenciamento</a>
                <a class="dropdown-item menuUserOp" href="#">Alterar Senha</a>
                <a class="dropdown-item menuUserOp" href="#">Sair</a>
                </div>
        </li>
        </ul>
    
    </nav>
    <aside id="sidebar">
        <div class="nomeEmpresa">
        <a href="">
            <img class="imgMenu" src="{{asset('assets/img/logo/logo_relatorio.png')}}" alt="">
            <span class="nomeUser">Agendamento</span>
            <span class="nomeUser">Perini</span>
        </a>
        </div>
            <ul>             


            </ul>
    </aside>
</div>

</header>




<body>
    

        <main class="py-4">
            @yield('content')
        </main>
    
</body>
</html>
